<!DOCTYPE html>
<html>

<head>
    <title>Success</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>

    <!--Styles and scripts-->
		<style>
	    /* Remove the navbar's default margin-bottom and rounded borders */
	    .navbar {margin-bottom: 0;border-radius: 0;}
	    /* Add a gray background color and some padding to the footer */   
	    .footer {background-color: #eeeeee;padding: 25px;}
	    .carousel-inner img {width: 100%;margin: auto;min-height: 200px;}
	    /* Hide the carousel text when the screen is less than 600 pixels wide */  
	    @media (max-width: 600px) {.carousel-caption {display: none;}}
	    div.footerContent {
		    background-color: #eeeeee;
		    margin: 0px;
		    clear: both;
		    border-top: 1px solid #ffffff;
			}
			div.contactSection h3, div.careerSection h3, div.relatedLinks h3, div.standardbankApp h3 {
			    font-size: 1.275em;
			    color: #000771;
			}
			.facebookico, .linkedinico, .ytico, .twitterico, .blogico {
			    display: block;
			    background: url('../../../img/SocialMedia.png') no-repeat;
			    float: left;
			    margin: 0 10px 0 0;
			    height: 36px;
			    width: 22px;
			}
			.facebookico {
			    background-position: 0px 0px;
			    width: 22px;
			    height: 22px;
			}
			.linkedinico {
			    background-position: -43px 0px;
			    width: 22px;
			    height: 22px;
			}
			.ytico {
			    background-position: -65px 0px;
			    width: 23px;
			    height: 22px;
			}
			.blogico {
			    background-position: -144px 0px;
			    width: 37px;
			    height: 22px;
			}
			.twitterico {
			    background-position: -21px 0px;
			    width: 23px;
			    height: 22px;
			}
		</style>
		
</head>

<body>
    <div>
        <div>
            <div class="container">
<!--Main Nav-->
                <nav class="navbar navbar-default">          
                    <div class="container-fluid">  
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li><a href="../../../Home.html">Home</a></li>
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Personal<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <p>&nbsp;Banking</p>
                                        </li>
                                        <li><a href="../../banking/credit-card.html">Credit Card</a></li>
                                        <li><a href="../../banking/current-account.html">Current Account</a></li>
                                        <li>
                                            <p>&nbsp;Borrowing</p>
                                        </li>
                                        <li><a href="../../borrowing/home-loans.html">Home Loans</a></li>
                                    </ul>
                                </li>
                                <li><a href="About.html">About</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
<!--End Main Nav-->

<!--Header-->
                <div class="jumbotron text-left" style="background-color:#337ab7; padding:0 0 0 10px; margin:0">
                    <img src="http://www.standardbank.co.za/standimg/SBG/FaceLift/Sitedrivenimages/OrganizationLogo-noSiteName.png" alt="Image">
                </div>
<!--End Header-->
                <br><hr/><br>
<!--Start Success status-->
               <div class="row">
                <div class="col-md-12" id="call_me_back_response">
                    <div class="col-md-14">
                        <div class="ts_call_me">
                            <p class="text-center response">
                                <img class="msg_icon" src="https://www.standardbank.co.za/standimg/SBG/FaceLift/images/icon_success.png" alt="">
                                <span>Thank you. Your details have been successfully submitted.</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
                <hr/><br>

<center><p style="height:20px"></p>
                    <div class="row main_banners">                          
                            <div class="col-sm-16">
                                <a href="#" alt="platinum_CC" data-toggle="modal" data-target="#CallMe">
                                    <img src="../../../img/hl-steps-f.png" />
                                </a>
                            </div>
                    </div>
            <p style="height:20px"></p>
</center>
<!--End Success status-->




	<div class="row footerContent">
	  <div class="container">
	    <div class="col-sm-3 contactSection">
	      <h3>Contact Standard Bank </h3>
	      <ul>
	        <li> <a> <span class="icon_telephone"> </span> 0860 123 000 </a> </li>
	        <li> <a id="email" href="mailto:information@standardbank.co.za"> <span class="icon_email--grey"> </span> Email Standard Bank </a> </li>
	      </ul>
	      <div class="socialIconset"> <a href="https://www.facebook.com/StandardBankSA"> <span class="facebookico"> </span> </a> <a href="http://www.twitter.com/standardbankza"> <span class="twitterico"> </span> </a><a href="http://www.linkedin.com/company/standard-bank-group"> <span class="linkedinico"> </span> </a><a href="http://www.youtube.com/standardbankgroup"> <span class="ytico"> </span> </a></div>
	    </div>
	    <div class="col-sm-3 careerSection">
	      <h3> Work with us </h3>
	      <p> Join our international team and you could move your career forward from the start. </p>
	      <div class="footermorebtn"> <a href="http://careers.standardbank.com/standimg/Careers/index.html" class="arrowLink"> View opportunities </a> </div>
	    </div>
	    <div class="col-sm-3 relatedLinks">
	      <h3> Related links </h3>
	      <ul>
	        <li> <a href="http://www.standardbank.com"> Standard Bank Global <span> </span> </a> </li>
	        <li> <a href="http://www.standardbank.co.za/standardbank/Personal/Self-service-banking/Security-Centre/About-Security-Centre"> Security Centre  <span> </span> </a> </li>
	        <li> <a href="/standardbank/About-Us/Site-specs"> Site Specs  <span> </span> </a> </li>
	        <li> <a href="http://www.sponsorships.standardbank.com/"> Standard Bank Arts  <span> </span> </a> </li>
	        <li> <a href="http://community.standardbank.co.za/blog "> Blog <span> </span> </a> </li>
	      </ul>
	    </div>
	    <div class="col-sm-3 standardbankApp">
	      <div class="Appbox">
	        <h3>The Banking App</h3>
	        <div class="appmessgaebox">
	          <p>Safer payments. <br>
	             Simpler transactions.<br>
	             Bank wherever you are.<br>
	            </p>
	          <div class="AppImg"> <img alt="Standard Bank App" src="http://www.standardbank.co.za/standimg/SBG/FaceLift/images/phone-app.png"> </div>
	          <div class="downloadbtn"> <a href="https://www.standardbank.co.za/standardbank/Personal/Self-service-banking/Self-Service-Channels/Mobile-App" class="arrowLink"> Find out more </a> </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>



            </div>            
        </div>
    </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--End Styles and scripts-->
</body>
</html>