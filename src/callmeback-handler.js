$(document).ready(function(){
	$('a[data-target="#CallMe"]').on('click', function() {
		var callmebacktype = $(this).data('creditcard')? "credit card" : "current account";
		if(callmebacktype == 'credit card'){
			$('h3.title', 'div#CallMe').html("Apply for a " + $(this).data('creditcard') + " Credit Card");
		} else {
			$('h3.title', 'div#CallMe').html("Apply for a " + $(this).data('currentaccount') + " Current Account");
		}
		
		// Apply the training fixes when enabled
		if(typeof window['appyFormNameOnCreditCardModal'] === 'function'){
			appyFormNameOnCreditCardModal('callmeback credit card ' + $(this).data('creditcard'));
		}
		
	});
	// 
	$('form#call').on('submit', function(e) {
		e.preventDefault();
		$('input', $(this)).val('');
		$('button.close', 'div#CallMe').trigger('click');
		$('div#call_me_back_response').removeClass('hide');
		// 
		if(typeof window['triggerAjaxComplete'] === 'function'){
			triggerAjaxComplete();
		}
		return false;
	});

	
});


