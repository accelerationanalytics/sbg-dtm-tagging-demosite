$(document).ready(function() {
	var navListItems = $('div.setup-panel div.stepwizard-step a');
	var allNextBtn = $('.nextBtn');
	var curStep = 1;
	var nextStep = 1;
	var formValid = true;

	$('a#hl-submit-application').on('click', function(e) {
		e.preventDefault();
		/* 001 TEST - Ajax submittion
		 */
		ajaxPostApplication();
		// 
		/* OR Test when submitting the form with a page refresh --> i.e. going to success page
		* (002 TEST - form submit)
		if($('input#applicatio-tnc').is(":checked")){
		    $('form#hl-main-form').submit();
		} else {
		    alert('You need to accep the terms and conditions.');
		}
		*/
	});
	// 
	navListItems.click(function(e) {
		e.preventDefault();

		nextStep = parseInt($(this).attr('href').replace(/[^0-9]+/g, ''));
		if ((curStep + 1) == nextStep) {
			nextStep--;
			$('div#step-' + curStep).find('button.nextBtn').eq(0).trigger('click');
		} else {
			showNextContainer();
		}

	});
	// 
	allNextBtn.click(function() {
		var curStep = $(this).closest(".setup-content");
		var curInputs = curStep.find("input[type='text'],input[type='url']");
		formValid = true;
		// 
		$(".form-group").removeClass("has-error");
		for (var i = 0; i < curInputs.length; i++) {
			if (!curInputs[i].validity.valid) {
				formValid = false;
				$(curInputs[i]).closest(".form-group").addClass("has-error");
			}
		}
		// 
		if (formValid) {
			nextStep++;
			showNextContainer();
		}
	});
	// 
	function showNextContainer() {
		$('div[id^="step-"]').each(function() {
			if ($(this).attr('id') == 'step-' + nextStep) {
				$(this).removeClass('hide');
			} else {
				$(this).addClass('hide');
			}
			curStep = nextStep;
		});


		navListItems.each(function() {
			if ($(this).attr('href') == '#step-' + nextStep) {
				$(this).removeClass('btn-default').addClass('btn-primary');
			} else {
				$(this).removeClass('btn-primary').addClass('btn-default');
			}
		});
	}

	/*
	 * Application submittion via Ajax
	 */
	function ajaxPostApplication() {
		$.ajax({
			"url": "post/ajaxhlApplicationProcess.php",
			"dataType": "json",
			"method": "POST",
			"data": {
				'tncs': $('input#applicatio-tnc').is(":checked")
			},
			success: function(respData) {
				// console.log(respData);
				if (respData.status == 1) {
					alert('Success');

				} else {
					console.log('Something went wrong', respData.errorCode, respData.message);
					alert('Yoopsie, ' + respData.message);
				}
			},
			error: function(error) {
				console.log('issue occured', error);
			}

		});
	}

});
